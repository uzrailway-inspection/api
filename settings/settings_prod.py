import django_heroku
from .settings import *

DEBUG = False
DEBUG_PROPAGATE_EXCEPTIONS = True

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = (
    'https://uzrailway-inspection-api.herokuapp.com',
    'https://uzrailway-inspection-ui.herokuapp.com',
    'https://xtapi.rwnadzor.uz',
    'https://xt.rwnadzor.uz',
    'http://localhost:8080',
)

django_heroku.settings(locals())
