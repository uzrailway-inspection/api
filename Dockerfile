FROM python:3.8.1-buster

ENV PROJECT_NAME uzrailway_api
ENV PROJECT_PATH /var/www/$PROJECT_NAME

RUN apt-get update &&\
    apt-get install -y binutils libproj-dev

RUN useradd -u 1000 app

RUN mkdir -p $PROJECT_PATH

# Permission project directory
RUN chmod -R 775 $PROJECT_PATH

# Cd to working directory
WORKDIR $PROJECT_PATH

# Copy requirements for catch
ADD ./requirements.txt $PROJECT_PATH
RUN pip3 install -r requirements.txt

# Copy project files
COPY --chown=app . $PROJECT_PATH

VOLUME $PROJECT_PATH/media
VOLUME $PROJECT_PATH/reports
VOLUME $PROJECT_PATH/static

WORKDIR $PROJECT_PATH
RUN chmod +x ./docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]
