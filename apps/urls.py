from django.urls import path, include

urlpatterns = [
    path('auth/', include(('apps.cauth.urls', 'apps.cauth'), 'cauth')),
    path('common/', include(('apps.common.urls', 'apps.common'), 'common')),
    path('company/',
         include(('apps.company.urls', 'apps.company'), 'company')),
]
