from django.db import models

from apps.shared.models import BaseModel


class Avatar(BaseModel):
    avatar = models.ImageField(upload_to='avatars/')

    def __str__(self):
        return self.avatar.path
