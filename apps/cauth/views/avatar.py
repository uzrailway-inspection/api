from rest_framework import viewsets, mixins

from apps.cauth.models import Avatar
from apps.cauth.serializer import AvatarSerializer


class AvatarViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    queryset = Avatar.objects.all()
    serializer_class = AvatarSerializer
