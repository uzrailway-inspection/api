from django.contrib.auth.models import Group
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, \
    DjangoModelPermissions, IsAdminUser

from apps.cauth.serializer import GroupSerializer


class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    search_fields = ('name',)
    ordering_fields = ('id', 'name',)
    permission_classes = (IsAdminUser, IsAuthenticated, DjangoModelPermissions)
