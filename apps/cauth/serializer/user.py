from django.contrib.auth.models import Permission, Group
from django.db import transaction

from rest_framework import serializers

from apps.cauth.models import User
from apps.common.serializer import RegionSerializer
from .avatar import AvatarSerializer
from .group import GroupSerializer
from .permission import PermissionSerializer


class MeUserSerializer(serializers.ModelSerializer):
    groups = serializers.PrimaryKeyRelatedField(
        queryset=Group.objects.all(),
        many=True,
        required=False
    )
    user_permissions = serializers.SlugRelatedField(
        queryset=Permission.objects.all(),
        slug_field='codename',
        many=True,
        required=False
    )
    avatar = AvatarSerializer(read_only=True)

    class Meta:
        model = User
        exclude = ('password',)

    def to_representation(self, instance):
        self.fields['avatar'] = AvatarSerializer()
        return super().to_representation(instance)


class RevisionUserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        exclude = (
            'password', 'groups', 'avatar', 'user_permissions', 'date_joined',
            'is_staff'
        )

    def get_full_name(self, obj):
        return "%s %s" % (obj.first_name, obj.last_name)


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'password',
            'is_superuser', 'groups', 'avatar', 'email',
            'is_active', 'region'
        )
        read_only_fields = (
            'modified_date', 'created_date', 'is_superuser', 'date_joined'
        )

    def to_representation(self, instance):
        self.fields['groups'] = GroupSerializer(many=True)
        self.fields['avatar'] = AvatarSerializer()
        self.fields['region'] = RegionSerializer(many=True)
        return super().to_representation(instance)

    @transaction.atomic
    def create(self, validated_data):
        groups = validated_data.pop('groups', None)
        region = validated_data.pop('region', None)
        password = validated_data.pop('password', None)
        new_user = User(**validated_data)

        if password:
            new_user.set_password(password)

        new_user.save()

        if region:
            new_user.region.set(region)

        if groups:
            group = groups[0]
            new_user.user_permissions.set(group.permissions.all())
            new_user.groups.set(groups)

        return new_user

    @transaction.atomic
    def update(self, instance, validated_data):
        groups = validated_data.pop('groups', None)
        password = validated_data.pop('password', None)
        update_user = super().update(instance, validated_data)

        if groups:
            group = groups[0]
            update_user.user_permissions.set(group.permissions.all())
            update_user.groups.set(groups)

        if password:
            update_user.set_password(password)
            update_user.save()

        return update_user


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'region',
            'groups', 'avatar', 'email', 'is_active', 'is_superuser'
        )
        read_only_fields = ('user_permissions',)

    def to_representation(self, instance):
        self.fields['groups'] = GroupSerializer(many=True)
        self.fields['user_permissions'] = PermissionSerializer(many=True)
        self.fields['avatar'] = AvatarSerializer()
        self.fields['region'] = RegionSerializer(many=True)
        return super().to_representation(instance)
