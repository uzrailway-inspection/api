# Django
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

# REST framework
from rest_framework.permissions import BasePermission

perm_suffix = {
    'list': 'list',
    'retrieve': 'retrieve',
    'create': 'create',
    'update': 'update',
    'partial_update': 'update',
    'destroy': 'destroy',
}


class PermissionNotFound(Exception):
    pass


class ActionPermission(BasePermission):
    """
    Allows access only to has perm users.
    """

    def get_codename(self, action, model):
        suffix = perm_suffix.get(action, action)
        return '%s_%s' % (suffix, model,)

    def get_content_type(self, model):
        return ContentType.objects.get_for_model(model)

    def get_perm(self, **kwargs):
        return Permission.objects.select_related().get(**kwargs)

    def get_action_perm(self, action, model):
        content_type = self.get_content_type(model)
        codename = self.get_codename(action, content_type.model)

        try:
            perm = self.get_perm(content_type=content_type, codename=codename)
            return '%s.%s' % (perm.content_type.app_label, codename,)
        except Permission.DoesNotExist:
            perm = '%s.%s' % (content_type.app_label, codename,)
            raise PermissionNotFound('Permission not found:' + perm)

    def has_permission(self, request, view):
        if view.action and request.user.is_authenticated:
            if request.user.is_superuser:
                return True
            model = view.get_queryset().model
            perm_name = self.get_action_perm(view.action, model)
            return request.user.has_perm(perm_name)

        return False
