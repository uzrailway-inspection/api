from django.urls import path, include

from rest_framework.routers import DefaultRouter

from apps.cauth.views import AuthUserView, MeUserView, GroupViewSet, \
    PermissionListView, UserViewSet, AvatarViewSet

app_name = 'apps.cauth'

user_router = DefaultRouter()
user_router.register(r'permissions', PermissionListView, basename='permission')
user_router.register(r'groups', GroupViewSet, basename='groups')
user_router.register(r'users', UserViewSet, basename='users')
user_router.register(r'avatar', AvatarViewSet, basename='avatar')

urlpatterns = [
    path('', include(user_router.urls)),
    path('me/', MeUserView.as_view(), name='me'),
    path('authenticate/', AuthUserView.as_view(), name='authenticate')
]
