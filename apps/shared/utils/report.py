import os
import time
import xlsxwriter
from django.conf import settings
from django.utils.dateparse import parse_datetime
from django.utils.dateformat import format


def set_head_format(workbook):
    head_format = workbook.add_format()
    head_format.set_bold()
    head_format.set_align('center')
    head_format.set_border(1)
    return head_format


def set_cell_format(workbook):
    cell_format = workbook.add_format()
    cell_format.set_border(1)
    cell_format.set_align('left')
    return cell_format


def export_rating(ratings):
    reports_dir = os.path.join(settings.BASE_DIR, 'reports')
    if not os.path.exists(reports_dir):
        os.makedirs(reports_dir)
    file_name = os.path.join(reports_dir, '%s%s' % (time.time_ns(), '.xlsx'))
    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()
    head_format = set_head_format(workbook)
    cell_format = set_cell_format(workbook)
    headers = {
        'A': {'name': 'Название компании', 'width': 30},
        'B': {'name': 'Главная организация', 'width': 30},
        'C': {'name': 'ИНН', 'width': 30},
        'D': {'name': 'Область', 'width': 30},
        'E': {'name': 'Станция', 'width': 30},
        'F': {'name': 'Сумма баллов', 'width': 30},
        'G': {'name': 'Категория', 'width': 30},
        'H': {'name': 'Активный', 'width': 30},
        'I': {'name': 'Изменение бала', 'width': 30},
        'J': {'name': 'Дата следующей проверки', 'width': 30},
    }
    row = 0
    col = 0
    for value in headers.values():
        worksheet.write(row, col, value['name'], head_format)
        worksheet.set_column(col, col, value['width'])
        col += 1

    row = 1
    col = 0

    for key, rating in enumerate(ratings):
        company = rating['company']
        rating_sum = rating.get('sum')
        difference = rating.get('difference')
        category = rating.get('category') or {}
        revision = rating.get('revision')
        next_revision_date = revision.get('next_revision_date')
        main_organization = company.get('main_organization') or {}
        is_active = 'да' if company.get('is_active') else 'нет'
        station = company.get('station') or {}
        district = company.get('district') or {}
        region = district.get('region') or {}

        worksheet.write(row, col, company.get('name_ru'), cell_format)
        worksheet.write(row, col + 1, main_organization.get('name_ru'),
                        cell_format)
        worksheet.write(row, col + 2, company.get('tax_id'), cell_format)
        worksheet.write(row, col + 3, region.get('name_ru'), cell_format)
        worksheet.write(row, col + 4, station.get('name_ru'), cell_format)
        worksheet.write(row, col + 5, rating_sum, cell_format)
        worksheet.write(row, col + 6, category.get('name_ru'), cell_format)
        worksheet.write(row, col + 7, is_active, cell_format)
        worksheet.write(row, col + 8, difference, cell_format)
        if next_revision_date:
            date = parse_datetime(next_revision_date)
            date_formatted = format(date, "d/m/yy")
            worksheet.write(row, col + 9, date_formatted, cell_format)
        row += 1

    workbook.close()

    return file_name
