from django_filters.filters import Filter
from rest_framework.exceptions import ValidationError


class ListFilter(Filter):
    def filter(self, queryset, value):
        if not value:
            return queryset
        list_values = value.split(',')
        if not all(item.isdigit() for item in list_values):
            raise ValidationError(
                'All values in %s are not integer' % str(list_values))
        return super().filter(queryset, list_values)
