from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from .models import District, Region

admin.site.register(District, TranslationAdmin)
admin.site.register(Region, TranslationAdmin)
