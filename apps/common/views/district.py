from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.common.filters import DistrictFilter
from apps.common.models import District
from apps.common.serializer import DistrictSerializer


class DistrictViewSet(viewsets.ModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    search_fields = ('name', 'name_uz', )
    filter_class = DistrictFilter

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        District.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
