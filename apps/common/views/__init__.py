from .region import RegionViewSet
from .district import DistrictViewSet

__all__ = ["RegionViewSet", "DistrictViewSet", ]
