import django_filters as filters

from apps.common.models import District


class DistrictFilter(filters.FilterSet):
    class Meta:
        model = District
        fields = ['region', ]
