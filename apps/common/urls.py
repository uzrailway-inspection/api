from django.urls import path, include
from rest_framework import routers

from apps.common.views import RegionViewSet, DistrictViewSet

app_name = 'apps.common'

router = routers.DefaultRouter()
router.register(r'region', RegionViewSet, basename='region')
router.register(r'district', DistrictViewSet, basename='district')

urlpatterns = [
    path('', include(router.urls)),
]
