from .region import RegionSerializer
from .district import DistrictSerializer

__all__ = ["RegionSerializer", "DistrictSerializer", ]
