from rest_framework import serializers

from apps.common.models import Region
from apps.shared.serializer import BaseModelSerializerMeta


class RegionSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Region
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)
