from rest_framework import serializers

from apps.common.models import District
from apps.shared.serializer import BaseModelSerializerMeta
from .region import RegionSerializer


class DistrictSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = District
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)

    def to_representation(self, instance):
        self.fields['region'] = RegionSerializer()
        return super().to_representation(instance)
