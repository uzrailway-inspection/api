from django.db import models

from apps.shared.models import BaseModel


class Region(BaseModel):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class District(BaseModel):
    name = models.CharField(max_length=64)
    region = models.ForeignKey('common.Region', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
