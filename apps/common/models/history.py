from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class History(models.Model):
    user = models.ForeignKey('cauth.User', on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=50)
    content_object = GenericForeignKey()
    created_date = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()

    class Meta:
        default_permissions = ('list', 'create', )
