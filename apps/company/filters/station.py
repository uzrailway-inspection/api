import django_filters as filters

from apps.company.models import Station


class StationFilter(filters.FilterSet):
    region = filters.NumberFilter(field_name='district__region')

    class Meta:
        model = Station
        fields = ['region', ]
