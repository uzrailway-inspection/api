import django_filters as filters

from apps.company.models import Factor


class FactorFilter(filters.FilterSet):
    exclude = filters.CharFilter(method='filter_exclude')

    class Meta:
        model = Factor
        fields = ['exclude']

    def filter_exclude(self, qs, name, value):
        if value:
            ids = value.split(',')
            return qs.exclude(pk__in=ids)
        return qs
