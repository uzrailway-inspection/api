import django_filters as filters

from apps.company.models import FactorValue


class FactorValueFilter(filters.FilterSet):
    class Meta:
        model = FactorValue
        fields = ['factor', ]
