import django_filters as filters

from apps.company.models import Company


class CompanyFilter(filters.FilterSet):
    region = filters.NumberFilter(field_name='district__region')

    class Meta:
        model = Company
        fields = ['district', "main_organization", "station",
                  "is_active", "region"]
