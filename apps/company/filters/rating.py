import datetime

import django_filters as filters
from django.db.models import Q

from apps.company.models import Rating

IMPORTANCE_CHOICES = (
    (0, 'green'),
    (1, 'yellow'),
    (2, 'red'),
)


class RatingFilter(filters.FilterSet):
    district = filters.NumberFilter(field_name='company__district')
    region = filters.NumberFilter(field_name='company__district__region')
    station = filters.NumberFilter(field_name='company__station')
    organization = filters.NumberFilter(
        field_name='company__main_organization')
    importance = filters.ChoiceFilter(method='filter_importance',
                                      choices=IMPORTANCE_CHOICES)

    class Meta:
        model = Rating
        fields = ['company', 'category', 'district', 'region', 'importance',
                  'station', 'organization']

    def filter_importance(self, qs, name, value):
        importance = int(value)
        now = datetime.datetime.now()
        if importance == 0:
            qs = qs.filter(Q(difference__lte=0) & ~Q(
                revision__next_revision_date__lt=now))
        if importance == 1:
            qs = qs.filter(Q(difference__gt=0) & Q(difference__lte=5) & ~Q(
                revision__next_revision_date__gt=now))
        if importance == 2:
            qs = qs.filter(Q(difference__gt=5) |
                           Q(revision__next_revision_date__lt=now))
        return qs.order_by('company__id').distinct('company__id')
