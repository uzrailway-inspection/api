import django_filters as filters

from apps.company.models import Revision


class RevisionFilter(filters.FilterSet):
    class Meta:
        model = Revision
        fields = ['rating', 'confirmed', 'rating__company']
