from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.company.views import StationViewSet, MainOrganizationViewSet, \
    CompanyViewSet, FactorViewSet, CategoryViewSet, RatingViewSet, \
    RevisionViewSet, ReportViewSet

app_name = 'apps.category'

user_router = DefaultRouter()
user_router.register(r'company', CompanyViewSet, basename='company')
user_router.register(r'station', StationViewSet, basename='station')
user_router.register(r'main-organization', MainOrganizationViewSet,
                     basename='main-organization')
user_router.register(r'factor', FactorViewSet, basename='factor')
user_router.register(r'category', CategoryViewSet, basename='category')
user_router.register(r'rating', RatingViewSet, basename='ball')
user_router.register(r'revision', RevisionViewSet, basename='revision')
user_router.register(r'report', ReportViewSet, basename='report')

urlpatterns = [
    path('', include(user_router.urls)),
]
