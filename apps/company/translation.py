from modeltranslation.translator import translator, TranslationOptions
from .models import Company, MainOrganization, Station, Category, Factor


class NameTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(Company, NameTranslationOptions)
translator.register(MainOrganization, NameTranslationOptions)
translator.register(Station, NameTranslationOptions)
translator.register(Category, NameTranslationOptions)
translator.register(Factor, NameTranslationOptions)
