from rest_framework import serializers

from apps.company.models import Factor
from apps.shared.serializer import BaseModelSerializerMeta


class FactorSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Factor
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)

    def create(self, validated_data):
        last = Factor.objects.order_by('-id').first()
        if last:
            validated_data['position'] = last.pk + 1
        else:
            validated_data['position'] = 0
        return super().create(validated_data)
