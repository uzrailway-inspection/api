from rest_framework import serializers

from apps.company.models import Rating, Revision
from apps.shared.serializer import BaseModelSerializerMeta
from apps.cauth.serializer import RevisionUserSerializer
from .company import CompanySerializer
from .category import CategorySerializer
from .factor_value import FactorValueSerializer


class RevisionSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Revision
        fields = None
        exclude = ('rating',)

    def to_representation(self, instance):
        self.fields['user'] = RevisionUserSerializer()
        return super().to_representation(instance)


class RatingSerializer(serializers.ModelSerializer):
    revision = serializers.SerializerMethodField(read_only=True)

    class Meta(BaseModelSerializerMeta):
        model = Rating
        read_only_fields = ('sum', 'difference', 'category',)

    def to_representation(self, instance):
        self.fields['company'] = CompanySerializer()
        self.fields['category'] = CategorySerializer()
        return super().to_representation(instance)

    def get_revision(self, obj):
        revision = Revision.objects.filter(rating=obj.pk,
                                           confirmed=True).order_by(
            '-id').first()
        return RevisionSerializer(revision).data


class RatingDetailSerializer(RatingSerializer):
    def to_representation(self, instance):
        self.fields['factor_values'] = FactorValueSerializer(many=True)
        return super().to_representation(instance)

    def get_revision(self, obj):
        revision = Revision.objects.filter(rating=obj.pk).order_by(
            '-id').first()
        if revision:
            return RevisionSerializer(revision).data


class RatingForInspectorSerializer(serializers.ModelSerializer):
    revision = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Rating
        read_only_fields = ('sum', 'category', 'created_date', 'modified_date')
        exclude = ('difference',)

    def to_representation(self, instance):
        self.fields['company'] = CompanySerializer()
        self.fields['category'] = CategorySerializer()
        return super().to_representation(instance)

    def get_revision(self, obj):
        revision = Revision.objects.filter(rating=obj.pk,
                                           confirmed=True).order_by(
            '-id').first()
        return RevisionSerializer(revision).data
