from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework import serializers

from apps.company.models import Revision, Rating, Company
from apps.shared.serializer import BaseModelSerializerMeta
from apps.company.serializer import RatingSerializer
from apps.cauth.serializer import RevisionUserSerializer


class RevisionCreateSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True, write_only=True)
    company = serializers.IntegerField(write_only=True, required=True)

    class Meta(BaseModelSerializerMeta):
        model = Revision
        read_only_fields = BaseModelSerializerMeta.read_only_fields + (
            'next_revision_date', 'confirmed', 'user', 'rating'
        )

    def validate_company(self, value):
        try:
            company = Company.objects.get(pk=value)
            rating = Rating.objects.get(company=value)
            revision = Revision.objects.filter(rating=rating, confirmed=False)
            if revision:
                raise serializers.ValidationError(
                    'У этой компании есть ревизия которая ещё не ' +
                    'подтверждена.')
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                'У этой компании не существует ни одного фактора')
        return company

    @transaction.atomic
    def create(self, validated_data):
        user = self.context['request'].user
        company = validated_data.pop('company')
        rating = Rating.objects.get(company=company.pk)
        is_active = validated_data.pop('is_active')
        validated_data['rating'] = rating
        company.is_active = is_active
        company.save()
        instance = Revision.objects.create(**validated_data, user=user)

        return instance

    def to_representation(self, instance):
        self.fields['user'] = RevisionUserSerializer()
        self.fields['rating'] = RatingSerializer()
        return super().to_representation(instance)


class RevisionUpdateSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Revision
        fields = ['revision_date', ]
        exclude = None
