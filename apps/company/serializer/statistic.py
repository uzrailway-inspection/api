from rest_framework import serializers

from apps.company.models import Statistic


class StatisticSerializer(serializers.ModelSerializer):
    rating_sum = serializers.FloatField(read_only=True)
    year = serializers.IntegerField(read_only=True)

    class Meta:
        model = Statistic
        fields = ('rating_sum', 'created_date', 'year')


class FullStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statistic
        fields = ('sum', 'created_date')


class FullStatisticRequestSerializer(serializers.Serializer):
    year = serializers.IntegerField(required=True)
