from django.db.models import Q
from django.urls import reverse
from rest_framework import serializers

from apps.company.models import Report, Revision, Rating
from apps.shared.serializer import BaseModelSerializerMeta
from apps.cauth.serializer import UserSerializer
from apps.company.serializer.rating import RevisionSerializer


class RatingSerializer(serializers.ModelSerializer):
    revision = serializers.SerializerMethodField(read_only=True)

    class Meta(BaseModelSerializerMeta):
        model = Rating
        read_only_fields = ('sum', 'difference', 'category',)

    def get_revision(self, obj):
        revision = Revision.objects.filter(
            Q(rating=obj.pk) & Q(confirmed=True)).order_by('-id').first()
        return RevisionSerializer(revision).data


class ReportSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Report

    def to_representation(self, instance):
        self.fields['ratings'] = RatingSerializer(many=True)
        self.fields['owner'] = UserSerializer()
        self.fields['file'] = serializers.SerializerMethodField()
        return super().to_representation(instance)

    def get_file(self, obj):
        return reverse('company:report-detail', kwargs=dict(pk=obj.pk))
