from datetime import datetime
from django.db import transaction
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from apps.company.models import Company, Category, FactorValue, Rating, \
    Statistic
from apps.shared.serializer import BaseModelSerializerMeta
from apps.common.serializer import DistrictSerializer
from .main_organization import MainOrganizationSerializer
from .station import StationSerializer
from .factor_value import FactorValueSerializer


class CompanySerializer(serializers.ModelSerializer):
    factor_values = FactorValueSerializer(many=True)
    is_factor_changed = serializers.BooleanField(default=True, write_only=True)

    class Meta(BaseModelSerializerMeta):
        model = Company
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)
        validators = [
            UniqueTogetherValidator(
                queryset=Company.objects.all(),
                fields=['tax_id']
            )
        ]

    def to_representation(self, instance):
        self.fields['main_organization'] = MainOrganizationSerializer()
        self.fields['district'] = DistrictSerializer()
        self.fields['station'] = StationSerializer()
        self.fields['factor_values'] = serializers.SerializerMethodField()
        return super().to_representation(instance)

    def get_factor_values(self, obj):
        rating = Rating.objects.filter(company=obj.pk).first()
        if rating:
            return FactorValueSerializer(rating.factor_values, many=True).data
        return []

    @transaction.atomic
    def create(self, validated_data):
        validated_data.pop('is_factor_changed')
        f_values = validated_data.pop('factor_values', [])
        instance = Company.objects.create(**validated_data)
        rating = Rating.objects.filter(company=instance).order_by(
            '-id').first()
        rating_sum = 0
        difference = 0
        factor_values = list()
        for f in f_values:
            factor_values.append(
                FactorValue(
                    factor=f.get('factor'),
                    value=f.get('value')
                )
            )
        factor_values = FactorValue.objects.bulk_create(factor_values)
        for factor_value in f_values:
            factor_val = factor_value.get('value', 0)
            rating_sum += factor_value.get('factor').calc_ball(factor_val)
        category = Category.objects.filter(min_rating_sum__lt=rating_sum,
                                           max_rating_sum__gte=rating_sum) \
            .first()

        if rating:
            difference = rating_sum - rating.sum
            rating = rating.update(sum=rating_sum,
                                   category=category,
                                   difference=difference)
        else:
            rating = Rating.objects.create(
                company=instance,
                sum=rating_sum,
                category=category,
                difference=difference
            )
        rating.factor_values.set(factor_values)
        rating.save()

        Statistic.objects.create(rating=rating, sum=rating_sum)

        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        is_factor_changed = validated_data.pop('is_factor_changed')
        f_values = validated_data.pop('factor_values', [])
        instance = super().update(instance, validated_data)
        rating = Rating.objects.filter(company=instance).order_by(
            '-id')
        if is_factor_changed:
            rating = rating.first()
            rating_sum = 0
            difference = 0
            factor_values = list()
            for f in f_values:
                factor_values.append(
                    FactorValue(
                        id=f.get('id'),
                        factor=f.get('factor'),
                        value=f.get('value')
                    )
                )
            factor_values = FactorValue.objects.bulk_create(factor_values)
            for factor_value in f_values:
                factor_val = factor_value.get('value', 0)
                rating_sum += factor_value.get('factor').calc_ball(factor_val)
            category = Category.objects.filter(min_rating_sum__lt=rating_sum,
                                               max_rating_sum__gte=rating_sum) \
                .first()

            if rating:
                difference = rating_sum - rating.sum
                rating.sum = rating_sum
                rating.category = category
                rating.difference = difference
            else:
                rating = Rating.objects.create(
                    company=instance,
                    sum=rating_sum,
                    category=category,
                    difference=difference
                )

            rating.factor_values.set(factor_values)
            rating.save()
            Statistic.objects.create(rating=rating, sum=rating_sum)
        else:
            rating.update(
                updated_date=datetime.now())

        return instance
