from rest_framework import serializers

from apps.company.models import FactorValue
from apps.shared.serializer import BaseModelSerializerMeta
from .factor import FactorSerializer


class FactorValueSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta(BaseModelSerializerMeta):
        model = FactorValue

    def to_representation(self, instance):
        self.fields['factor'] = FactorSerializer()
        return super().to_representation(instance)
