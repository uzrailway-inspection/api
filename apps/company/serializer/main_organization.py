from rest_framework import serializers

from apps.company.models import MainOrganization
from apps.shared.serializer import BaseModelSerializerMeta


class MainOrganizationSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = MainOrganization
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)
