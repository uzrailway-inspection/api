from rest_framework import serializers

from apps.company.models import Station
from apps.shared.serializer import BaseModelSerializerMeta
from apps.common.serializer import DistrictSerializer


class StationSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Station
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)

    def to_representation(self, instance):
        self.fields['district'] = DistrictSerializer()
        return super().to_representation(instance)
