from rest_framework import serializers

from apps.company.models import Category
from apps.shared.serializer import BaseModelSerializerMeta


class CategorySerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Category
        read_only_fields = BaseModelSerializerMeta.read_only_fields + ('name',)
