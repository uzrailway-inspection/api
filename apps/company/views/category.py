from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import Category
from apps.company.serializer import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    search_fields = ('name', 'name_uz', 'name_ru', )

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        Category.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
