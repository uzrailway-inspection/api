from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from apps.company.models import Revision
from apps.company.serializer import RevisionCreateSerializer, \
    RevisionUpdateSerializer
from apps.company.filters.revision import RevisionFilter
from apps.cauth.permissions import ActionPermission


class RevisionViewSet(ListAPIView, CreateAPIView, ViewSet):
    queryset = Revision.objects.last_revision()
    serializer_class = RevisionCreateSerializer
    search_fields = ('rating__company__name_uz', 'rating__company__name_ru',
                     'rating__company__tax_id',)
    filter_class = RevisionFilter

    def get_serializer_class(self):
        serializer = super().get_serializer_class()
        if self.action in ['update', 'partial_update']:
            serializer = RevisionUpdateSerializer
        return serializer

    @action(methods=['post'], detail=True, url_path='confirm',
            url_name='confirm', permission_classes=[ActionPermission])
    def confirm(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.confirm()
        instance.rating.difference = 0
        serializer = self.serializer_class(instance)

        return Response(serializer.data)

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        Revision.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
