from django.db import transaction
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import Factor
from apps.company.serializer import FactorSerializer
from apps.company.filters.factor import FactorFilter


class FactorViewSet(viewsets.ModelViewSet):
    queryset = Factor.objects.all().order_by('-position')
    serializer_class = FactorSerializer
    search_fields = ('name', 'name_uz', 'tax_id',)
    filter_class = FactorFilter

    @action(['post'], url_path='change-position', detail=True)
    @transaction.atomic
    def change_position(self, request, pk=None):
        second_pk = request.data.get('replace_id')
        first_factor = self.get_object()
        second_factor = Factor.objects.get(pk=second_pk)
        serializer = self.get_serializer(first_factor)
        if second_factor:
            last_position = first_factor.position
            first_factor.position = second_factor.position
            second_factor.position = last_position
            first_factor.save()
            second_factor.save()
        return Response(serializer.data)

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        Factor.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
