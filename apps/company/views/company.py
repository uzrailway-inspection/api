from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import Company, Rating
from apps.company.serializer import CompanySerializer
from apps.company.filters import CompanyFilter


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    search_fields = ('name', 'name_uz', 'tax_id',)
    filter_class = CompanyFilter

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        rating = Rating.objects.filter(company=instance).first()
        for factor_value in rating.factor_values.all():
            factor_value.delete()
        rating.delete()
        return super().destroy(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        if user.is_superuser:
            return queryset
        user_regions = user.region.all()
        return queryset.filter(district__region__in=user_regions)

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        Company.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
