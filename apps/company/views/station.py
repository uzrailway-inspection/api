from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import Station
from apps.company.filters import StationFilter
from apps.company.serializer import StationSerializer


class StationViewSet(viewsets.ModelViewSet):
    queryset = Station.objects.all()
    serializer_class = StationSerializer
    search_fields = ('name', 'name_uz',)
    filter_class = StationFilter

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        Station.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
