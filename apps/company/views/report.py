import os

from django.http import FileResponse
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from apps.shared.serializer import ListIdSerializer
from apps.company.models import Report, Rating
from apps.company.serializer import ReportSerializer, RatingSerializer
from apps.shared.utils import export_rating


class ReportViewSet(ListAPIView, CreateAPIView, ViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    search_fields = ('ratings__company__name_ru', 'ratings__company__name_uz',
                     'owner__username', 'owner__first_name',
                     'owner__last_name')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        file_path = instance.file.name
        try:
            file = open(file_path, 'rb')
            response = FileResponse(file)
            response['Content-Type'] = 'application/vnd.openxmlformats-off' + \
                                       'icedocument.spreadsheetml.sheet'
            response['Content-Disposition'] = 'attachment; filename="%s"' % (
                file.name)
            response['Content-Length'] = os.path.getsize(file_path)
            return response
        except IOError:
            return Response(
                data=dict(error='Файл удалён или не существует!'),
                status=status.HTTP_404_NOT_FOUND
            )

    def create(self, request, *args, **kwargs):
        ids = request.data.get('ids', [])
        id_serializer = ListIdSerializer(data=request.data)
        if id_serializer.is_valid():
            ratings = Rating.objects.filter(pk__in=ids)
            ratings_data = RatingSerializer(ratings, many=True).data
            file = export_rating(ratings_data)
            owner = request.user
            report = Report.objects.create(file=file, owner=owner)
            report.ratings.set(ratings)
            report.save()
            serializer = self.get_serializer(report)
            return Response(status=status.HTTP_201_CREATED,
                            data=serializer.data)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=id_serializer.errors)
