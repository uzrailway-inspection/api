from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import FactorValue
from apps.company.filters import FactorValueFilter
from apps.company.serializer import FactorValueSerializer


class FactorValueViewSet(viewsets.ModelViewSet):
    queryset = FactorValue.objects.all()
    serializer_class = FactorValueSerializer
    filter_class = FactorValueFilter

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        FactorValue.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
