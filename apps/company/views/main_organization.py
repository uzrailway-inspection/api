from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.company.models import MainOrganization
from apps.company.serializer import MainOrganizationSerializer


class MainOrganizationViewSet(viewsets.ModelViewSet):
    queryset = MainOrganization.objects.all()
    serializer_class = MainOrganizationSerializer
    search_fields = ('name', 'name_uz', 'name_ru', )

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        MainOrganization.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
