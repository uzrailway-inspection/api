from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.company.models import Rating, Statistic
from apps.company.filters import RatingFilter
from apps.company.serializer import RatingSerializer, RatingDetailSerializer, \
    StatisticSerializer, FullStatisticRequestSerializer, \
    FullStatisticSerializer, RatingForInspectorSerializer


class RatingViewSet(mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    filter_class = RatingFilter
    search_fields = ('company__name_ru', 'company__name_uz',)

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.action == 'retrieve':
            serializer_class = RatingDetailSerializer
        return serializer_class

    def get_queryset(self):
        return super().get_queryset().order_by('-sum', '-difference')

    @action(['get'], url_path='get-statistics', detail=True)
    def get_statistics(self, request, pk):
        statistics = Statistic.objects.get_statistic(pk)
        data = StatisticSerializer(statistics, many=True).data
        return Response(data)

    @action(['get'], url_path='get-full-statistics', detail=True)
    def get_full_statistics(self, request, pk):
        serializer = FullStatisticRequestSerializer(data=request.query_params)
        if serializer.is_valid():
            year = serializer.data.get('year')
            statistics = Statistic.objects.get_full_statistic(pk, year)
            data = FullStatisticSerializer(statistics, many=True).data
            return Response(data)

    @action(['get'], url_path='get-rating-for-inspector', detail=False)
    def get_rating_for_inspector(self, request):
        user = request.user
        queryset = self.filter_queryset(super().get_queryset())
        if not user.is_superuser:
            user_regions = user.region.all()
            queryset = queryset.filter(
                company__district__region__in=user_regions)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RatingForInspectorSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = RatingForInspectorSerializer(queryset, many=True)
        return Response(serializer.data)
