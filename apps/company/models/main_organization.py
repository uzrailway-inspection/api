from django.db import models

from apps.shared.models import BaseModel


class MainOrganization(BaseModel):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name
