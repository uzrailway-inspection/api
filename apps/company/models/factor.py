from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.validators import MinValueValidator
from django.db import models

from apps.shared.models import BaseModel
from apps.shared.models.base import DEFAULT_PERMISSIONS


class Factor(BaseModel):
    name = models.CharField(max_length=128)
    is_interval = models.BooleanField(default=True)
    conditions = ArrayField(JSONField(), default=list)
    position = models.PositiveIntegerField(validators=[MinValueValidator(0)],
                                           default=0)

    class Meta(BaseModel.Meta):
        default_permissions = DEFAULT_PERMISSIONS + (
            'change_position',
        )

    def __str__(self):
        return self.name

    def calc_ball(self, val):
        conditions = self.conditions
        is_interval = self.is_interval
        if not is_interval:
            value = conditions[0].get('value', 0)
            logic = conditions[0].get('logic', False)
            if logic:
                return value
            return value * val
        for condition in conditions:
            min_val = condition.get('min', 0)
            max_val = condition.get('max', 0)
            value = condition.get('value', 0)
            if min_val and max_val:
                if min_val <= val < max_val:
                    return value
        return 0
