from django.db import models

from apps.shared.models import BaseModel


class Report(BaseModel):
    ratings = models.ManyToManyField('company.Rating')
    file = models.FileField()
    owner = models.ForeignKey('cauth.User', on_delete=models.CASCADE)
