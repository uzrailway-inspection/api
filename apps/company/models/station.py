from django.db import models

from apps.shared.models import BaseModel


class Station(BaseModel):
    name = models.CharField(max_length=128, unique=True)
    district = models.ForeignKey('common.District', null=True,
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return self.name
