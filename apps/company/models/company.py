from django.db import models

from apps.shared.models import BaseModel


class Company(BaseModel):
    name = models.CharField(max_length=128)
    tax_id = models.IntegerField(unique=True)
    main_organization = models.ForeignKey('company.MainOrganization',
                                          null=True, on_delete=models.SET_NULL)
    district = models.ForeignKey('common.District', null=True,
                                 on_delete=models.SET_NULL)
    station = models.ForeignKey('company.Station', null=True,
                                on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
