from django.db import models
from django.db.models import Avg
from django.db.models.functions import ExtractYear


class StaticQuerySet(models.QuerySet):
    def get_statistic(self, rating):
        return self.filter(rating__pk=rating).annotate(
            year=ExtractYear('created_date')).values('year').annotate(
            rating_sum=Avg('sum')).order_by('year')

    def get_full_statistic(self, rating, year):
        return self.filter(rating__pk=rating).filter(
            created_date__year=year).order_by('created_date')


class StatisticManager(models.Manager):
    def get_queryset(self):
        return StaticQuerySet(self.model, using=self._db)

    def get_statistic(self, rating_pk):
        return self.get_queryset().get_statistic(rating_pk)

    def get_full_statistic(self, rating_pk, year):
        return self.get_queryset().get_full_statistic(rating_pk, year)


class Statistic(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    rating = models.ForeignKey('company.Rating', on_delete=models.CASCADE)
    sum = models.IntegerField()

    objects = StatisticManager()

    class Meta:
        ordering = ('-id',)
