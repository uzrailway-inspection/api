from django.db import models

from apps.shared.models import BaseModel


class Category(BaseModel):
    name = models.CharField(max_length=128)
    min_rating_sum = models.IntegerField()
    max_rating_sum = models.IntegerField()

    def __str__(self):
        return self.name
