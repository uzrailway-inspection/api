from django.db import models

from apps.shared.models import BaseModel
from apps.shared.models.base import DEFAULT_PERMISSIONS


class FactorValue(BaseModel):
    factor = models.ForeignKey('company.Factor', on_delete=models.CASCADE)
    value = models.FloatField(default=0)

    def __str__(self):
        return self.value
