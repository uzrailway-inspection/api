from django.db import models

from apps.shared.models import BaseModel
from apps.shared.models.base import DEFAULT_PERMISSIONS


class Rating(BaseModel):
    sum = models.FloatField()
    difference = models.FloatField(default=0)
    factor_values = models.ManyToManyField('company.FactorValue')
    company = models.OneToOneField('company.Company', unique=True,
                                   on_delete=models.CASCADE)
    category = models.ForeignKey('company.Category', null=True,
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.company.name

    class Meta(BaseModel.Meta):
        default_permissions = DEFAULT_PERMISSIONS + (
            'get_statistics', 'get_full_statistics',
            'get_rating_for_inspector',)
