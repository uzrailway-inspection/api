from datetime import timedelta

from django.db import models
from django.db.models import Subquery
from django.db.models.signals import pre_save

from apps.shared.models import BaseModel
from apps.shared.models.base import DEFAULT_PERMISSIONS


class RevisionQuerySet(models.QuerySet):
    def last_revision(self):
        return self.filter(
            pk__in=Subquery(
                self.order_by('rating__id', '-id').distinct(
                    'rating__id').values('pk')
            )).order_by('-id')


class RevisionManager(models.Manager):
    def get_queryset(self):
        return RevisionQuerySet(self.model, using=self._db)

    def last_revision(self):
        return self.get_queryset().last_revision()


class Revision(BaseModel):
    user = models.ForeignKey('cauth.User', on_delete=models.CASCADE)
    rating = models.ForeignKey('company.Rating', related_name='revision',
                               on_delete=models.CASCADE)
    revision_date = models.DateField()
    next_revision_date = models.DateField()
    confirmed = models.BooleanField(default=False)

    objects = RevisionManager()

    class Meta(BaseModel.Meta):
        default_permissions = DEFAULT_PERMISSIONS + (
            'confirm',
        )

    def __str__(self):
        return self.rating.company.name

    def confirm(self):
        self.confirmed = True
        self.save()

    @staticmethod
    def update_next_revision_date(sender, instance, **kwargs):
        instance.next_revision_date = instance.revision_date + timedelta(
            days=365)


pre_save.connect(Revision.update_next_revision_date,
                 sender=Revision)
