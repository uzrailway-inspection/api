# Generated by Django 3.0.3 on 2020-03-07 02:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('common', '0003_history'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainOrganization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=128, unique=True)),
            ],
            options={
                'ordering': ('-id',),
                'abstract': False,
                'default_permissions': ('list', 'retrieve', 'create', 'update', 'destroy'),
            },
        ),
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=128, unique=True)),
            ],
            options={
                'ordering': ('-id',),
                'abstract': False,
                'default_permissions': ('list', 'retrieve', 'create', 'update', 'destroy'),
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=128)),
                ('tax_id', models.IntegerField(unique=True)),
                ('is_active', models.BooleanField(default=True)),
                ('district', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='common.District')),
                ('main_organization', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.MainOrganization')),
                ('station', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.Station')),
            ],
            options={
                'ordering': ('-id',),
                'abstract': False,
                'default_permissions': ('list', 'retrieve', 'create', 'update', 'destroy'),
            },
        ),
    ]
