# Generated by Django 3.0.3 on 2020-04-25 17:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0022_auto_20200414_0123'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rating',
            options={'default_permissions': ('list', 'retrieve', 'create', 'update', 'destroy', 'destroy_bulk', 'get_statistics'), 'ordering': ('-id',)},
        ),
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('sum', models.IntegerField()),
                ('rating', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Rating')),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
    ]
