#!/bin/bash
set -e

# Go to directory
cd "$PROJECT_PATH"

# shellcheck disable=SC2039
if [ "$DJANGO_SETTINGS_MODULE" == "settings.settings_test" ]; then
  echo "Run testing mode"

  # Run test
  python3 manage.py test --noinput
  exit
else
  echo "Run production mode"
  python3 manage.py migrate

  python3 manage.py collectstatic --noinput

  chmod 775 -R "$PROJECT_PATH"/media
  chown app:app -R "$PROJECT_PATH"/media
  chmod 775 -R "$PROJECT_PATH"/static
  chown app:app -R "$PROJECT_PATH"/static
  chmod 775 -R "$PROJECT_PATH"/reports
  chown app:app -R "$PROJECT_PATH"/reports

  exec gunicorn settings.wsgi -b 0.0.0.0:8000
  exit
fi
